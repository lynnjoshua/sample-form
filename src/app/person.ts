export interface Person {
  firstName: string;
  lastName: string;
  address: string;
  sports: string;
}

export interface Address {
  street: string;
  city: string;
  country: string;
}

export interface Sport {
  name: string;
  isActive: boolean;
}
