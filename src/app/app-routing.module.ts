import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(mod => mod.HomeModule)
  },
  {
    path: 'sample-page',
    loadChildren: () => import('./sample-page/sample-page.module').then(mod => mod.SamplePageModule)
  },
  {
    path: 'form',
    loadChildren: () => import('./form/form.module').then(mod => mod.FormModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
