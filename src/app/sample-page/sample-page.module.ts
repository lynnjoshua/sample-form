import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SamplePageRoutingModule } from './sample-page-routing.module';
import { SampleComponent } from './containers/sample/sample.component';


@NgModule({
  declarations: [SampleComponent],
  imports: [
    CommonModule,
    SamplePageRoutingModule
  ]
})
export class SamplePageModule { }
