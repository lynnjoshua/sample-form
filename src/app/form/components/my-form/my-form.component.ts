import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Form, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Person} from '../../../person';

@Component({
  selector: 'app-my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.scss']
})
export class MyFormComponent implements OnInit {

  @Output() formSubmit: EventEmitter<Person> = new EventEmitter<any>();
  @Input() formData: Person;

  form: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.createForm();
    this.patchForm();
  }

  createForm(): FormGroup {
    return this.fb.group({
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      address: this.createAddressGroup(),
      sports: this.fb.array([])
    });
  }

  createAddressGroup(): FormGroup {
    return this.fb.group({
      street: [null, [Validators.required]],
      city: [null, [Validators.required]],
      country: [null, [Validators.required]],
    });
  }

  createSportGroup(): FormGroup {
    return this.fb.group({
      name: [null, [Validators.required]],
      isActive: false
    });
  }

  addSport() {
    this.sports.push(this.createSportGroup());
  }

  removeSport(index) {
    this.sports.removeAt(index);
  }

  patchForm() {
    if (this.formData) {
      this.form.patchValue(this.formData);
    }
  }

  onsubmit() {
    const person: Person = this.form.value;

    this.formSubmit.emit(person);
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get address() {
    return this.form.get('address');
  }

  get street() {
    return this.address.get('street');
  }

  get city() {
    return this.address.get('city');
  }

  get country() {
    return this.address.get('country');
  }

  get sports() {
    return this.form.get('sports') as FormArray;
  }
}
